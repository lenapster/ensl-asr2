#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

void byebye1(){
	printf("Bye bye 1.\n");
}

void byebye2(){
        printf("Bye bye 2.\n");
}

void mon_h(int sig){
	int status,rc;
	printf("ccccccccccccccccc\n");
	rc=waitpid(-1,&status,WNOHANG);
	if(rc<0)
		printf("rrrrrrrrrrrrrr\n");
	else
		printf("tttttttttttttttttttt  %d\n",rc);
}

int main(int argc, char** argv){
	int pid,rc=0, wstatus;
	printf("Je démarre\n.");
	
	atexit(&byebye1);
	atexit(&byebye2);
	signal(SIGCHLD,mon_h);
	pid=fork();
	if(pid<0){
		printf("Un probleme\n");
		exit(1);
	}else{
		if(pid==0){
		
			rc=1;
			printf("Je suis le fils (mon-PID=%d), père-PIF=%d. (rc=%d).\n",getpid(),getppid(),rc);
			sleep(5);
				
			/*	
		        char *args[4]={"/bin/echo","$PWD",NULL};//Le dernier NULL est nécessaire.		
			char *argv[2]={"export PWD=/home/napster",NULL};
			printf("--------------------------\n");
			execve(args[0],args,argv);
			*/
			printf("--------------------------\n");//Va-t-il s'afficher?
		}else{
			
			//1
                        printf("(rc=%d) Je suis le père (mon-PID=%d), fils-PID=%d\n",rc,getpid(),pid);
			
			while(1){
				rc=wait(&wstatus);
				printf("ffffffffffffffff %d\n",rc);	
				//continue;
				//rc=waitpid(-1, &wstatus, WNOHANG);
				if(rc<0){
					printf("Le wait n'a pas fonctionné.\n");
					exit(1);
				}else if(rc==0){
					//printf("Aucun événement chez mes fils.\n");
					continue;
				}else{
					printf("Le PID du fils qui vient de nous débloquer est %d.\n",rc);
					rc=0;
				}
				if(WIFEXITED(wstatus)){
					printf("Le fils s'est terminé par lui même, avec comme retour %d (identique à rc dans le fils).\n",WEXITSTATUS(wstatus));
					break;
				}else if(WIFSIGNALED(wstatus)){
					printf("Le fils a été tué par le signal %d.\n",WTERMSIG(wstatus));
					break;
				}else if(WIFSTOPPED(wstatus)){
        	                        printf("Le fils a été stopé par le signal %d.\n",WSTOPSIG(wstatus));
					//Pas de break, faut retourner attendre, permet d'éviter que le fils reste zombie lorsqu'il se terminera!
                	        }else if(WIFCONTINUED(wstatus)){
					printf("Le fils est sorti de sa pause.\n");
					//Pas de break, faut retourner attendre, permet d'éviter que le fils reste zombie lorsqu'il se terminera!
				}else{
					printf("Le fils a un autre soucis.\n");
				}
				
			}
			
		}
	}

	printf("Hello (mon-PID=%d)\n",getpid());
	return rc;
}
