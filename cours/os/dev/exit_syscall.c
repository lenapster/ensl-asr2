#include <stdio.h>
#include <stdlib.h>


void byebye(){
        printf("Bye bye.\n");
}


int main(int argc, char *argv[])
{
  unsigned int syscall_nr = 1;
  int exit_status = 42;

    
  atexit(&byebye);
  

  printf("Exécuter la commande 'echo $?', qui devrait afficher 42.\n");
  exit(0);
  printf("Cette ligne ne s'affichera pas.\n");  
}


