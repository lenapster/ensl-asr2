#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

void *printme(){
	printf("Hello world\n");
	return NULL;
}

int main(){
	pthread_t tcb;
	void *status;

	if (pthread_create(&tcb, NULL, printme, NULL) != 0) {
		perror("pthread_create");
		exit(1);
	}
	if (pthread_join(tcb, &status) != 0) {
		perror("pthread_join"); exit(1);
	}
	return 0;
}


