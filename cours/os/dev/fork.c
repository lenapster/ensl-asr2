#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

void byebye(){
        printf("Bye bye.\n");
}

int main(int argc, char** argv){
	int pid;

  atexit(&byebye);


	pid=fork();
	//////////////////////////
	if(pid<0){
		printf("error fork\n");
		exit(0);		
	}else{
		if(pid==0){
			char *cmd[2]={"/bin/ls",NULL};
			execve("/bin/ls",cmd,NULL);
                        printf("Je suis le fils %d et ma maman est %d\n",getpid(), getppid());
			
		}else{

			printf("je suis la mère (%d) et mon fils est %d\n",getpid(),pid);
		}
	}
		
}

